"""monitoring URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from core.views import call_response
from reports.views import (
    LoginView, 
    DashboardView,
    HistoricalView
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', LoginView.as_view(), name='login'),
    url(r'^dashboard/', DashboardView.as_view(), name='dashboard'),
    url(r'^historical/', HistoricalView.as_view(), name='historical'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^core/', include('core.urls', namespace='core')),
    url(r'^call-response', call_response)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
