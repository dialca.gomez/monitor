# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import xlwt

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from core.models import Record, Device, Pond


class LoginView(TemplateView):
    template_name = 'auth/sign-in.html'

class DashboardView(TemplateView):
    template_name = 'panel/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['last_oxygen_record'] = Record.objects.filter(device__parameter__measurement_type='OX').last()
        context['last_temperature_record'] = Record.objects.filter(device__parameter__measurement_type='TE').last()
        return context

class HistoricalView(TemplateView):
    template_name = 'panel/historical.html'

    def get_context_data(self, **kwargs):
        context = super(HistoricalView, self).get_context_data(**kwargs)
        context['devices'] = Device.objects.all()
        context['ponds'] = Pond.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="reporte.xls"'

        pond = Pond.objects.get(pk=request.POST['pond'])
        device = Device.objects.get(pk=request.POST['device'])
        records = Record.objects.filter(
            device=device,
            date__gte=request.POST['start_date'],
            date__lte=request.POST['end_date']
        ).order_by('-date').values_list('device', 'value')

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Registros')

        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['Dispositivo', 'Valor marcado']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        font_style = xlwt.XFStyle()

        for row in records:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)

        return response