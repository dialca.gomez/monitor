# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

from rest_framework import generics
from .models import Parameter
from .serializers import ParameterSerializer


class ParameterList(generics.ListAPIView):
    queryset = Parameter.objects.all()
    serializer_class = ParameterSerializer 

@csrf_exempt
def call_response(request):
    return JsonResponse([
        {
            "action": "talk",
            "voiceName": "Penelope",
            "text": 'La temperatura del estanque 1 se encuentra fuera de los límites permitidos'
        }
    ], safe=False)

