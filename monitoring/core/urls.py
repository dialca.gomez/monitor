
from django.conf.urls import url, include
from .views import ParameterList


urlpatterns = [
    url(r'^parameters/', ParameterList.as_view(), name="parameter-list")
]
