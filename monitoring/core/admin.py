# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import (
    Parameter,
    Device, 
    Record,
    Pond
)


@admin.register(Parameter)
class ParameterAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">settings_remote</i>'
    list_display = ('pk', 'measurement_type', 'min_quantity', 'max_quantity', 'cellphone')

@admin.register(Pond)
class PondAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">album</i>'
    list_display = ('pk', 'name')

@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">devices_other</i>'
    list_display = ('pk', 'pond', 'name', 'parameter')

@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">list</i>'
    list_display = ('pk', 'date', 'device', 'get_parameter', 'value')