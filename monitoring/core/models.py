# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import nexmo
from django.conf import settings
from django.db import models


class Parameter(models.Model):
    """Almacena los parámetros de configuración de los sensores
    """

    TEMPERATURE = 'TE'
    OXYGEN = 'OX'
    MEASUREMENT_TYPE_CHOICES = (
        (TEMPERATURE, 'Temperatura'),
        (OXYGEN, 'Oxigeno'),
    )

    measurement_type = models.CharField('Unidad de medida', max_length=2, choices=MEASUREMENT_TYPE_CHOICES, unique=True)
    min_quantity = models.PositiveIntegerField('Cantidad mínima')
    max_quantity = models.PositiveIntegerField('Cantidad máxima')
    cellphone = models.CharField(
        'Teléfono celular de notificación',
        max_length=15,
        null=True,
        blank=True,
        help_text="El formato del número de celular debe ser 57 + NUMERO (Ejemplo: 573201234567)"
    )

    class Meta:
        verbose_name = "Parámetro"

    def __str__(self):
        if self.measurement_type == self.TEMPERATURE:
            return 'Temperatura'
        else:
            return 'Oxígeno'
    
    def get_gram_indicator(self):
        if self.measurement_type == self.TEMPERATURE:
            return 'La'
        else:
            return 'El'

class Pond(models.Model):
    name = models.CharField('Nombre', max_length=255)

    class Meta:
        verbose_name = "Estanque"
    
    def __str__(self):
        return str(self.name)

class Device(models.Model):
    pond = models.ForeignKey(Pond, null=True, verbose_name="Estanque")
    name = models.CharField('Nombre de dispositivo', max_length=255)
    parameter = models.ForeignKey(Parameter, verbose_name="Tipo de parámetro", null=True, blank=True)

    class Meta:
        verbose_name = "Dispositivo"

    def __str__(self):
        return '{} ({})'.format(self.name, self.parameter)

class Record(models.Model):
    device = models.ForeignKey(Device, verbose_name="Dispositivo")
    value =  models.IntegerField('Valor')
    date = models.DateTimeField('Fecha', auto_now_add=True)

    class Meta:
        verbose_name = "Registro"

    def __str__(self):
        return 'Registro de {} del {}'.format(self.device.parameter, self.date)
    
    def get_parameter(self):
        return self.device.parameter
    
    def has_passed_range(self):
        if self.device.parameter.measurement_type == Parameter.TEMPERATURE:
            measurement_range = Parameter.objects.get(measurement_type=Parameter.TEMPERATURE)
        else:
            measurement_range = Parameter.objects.get(measurement_type=Parameter.OXYGEN)

        if measurement_range.min_quantity <= self.value <= measurement_range.max_quantity:
            return False
        return True

    def save(self, *args, **kwargs):

        if self.has_passed_range():
            if self.device.parameter.cellphone:
                PRIVATE_KEY = open(settings.BASE_DIR + '/private.key', 'r').read()
                client = nexmo.Client(
                    key='abdce778',
                    secret='CgN76CLswAQf14yr',
                    application_id='87feb32b-c260-4cc8-b65c-9648d9a3d1bd',
                    private_key=PRIVATE_KEY
                )

                message = '{} {} del estanque {} no se encuentra entre los límites permitidos'.format(
                    self.device.parameter.get_gram_indicator(),
                    self.device.parameter,
                    self.device.pk
                )

                client.send_message({
                    'from': 'Nexmo',
                    'to': self.device.parameter.cellphone,
                    'text': message
                })

                response = client.create_call({
                    'to': [{'type': 'phone', 'number': self.device.parameter.cellphone}],
                    'from': {'type': 'phone', 'number': '12345678901'},
                    'answer_url': ['http://af21e8d6.ngrok.io/call-response']
                })

        super(Record, self).save(*args, **kwargs)

        