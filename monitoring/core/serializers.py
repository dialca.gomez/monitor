from rest_framework import serializers
from .models import Parameter


class ParameterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parameter
        fields = ('pk', 'measurement_type', 'min_quantity', 'max_quantity')