# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'
    verbose_name = 'Configuracion del sistema'
    icon = '<i class="material-icons">settings applications</i>'
